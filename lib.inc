%define SYS_WRITE 1
%define SYS_READ  0
%define SYS_EXIT  60
%define STDOUT    1
%define STDIN     0

section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je  .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push    rdi
    call    string_length
    pop     rsi               ; ptr
    mov     rdx, rax          ; length
    mov     rax, SYS_WRITE
    mov     rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov  rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
    push    rdi
    mov     rax, SYS_WRITE
    mov     rsi, rsp
    mov     rdi, STDOUT
    mov     rdx, 1
    syscall
    pop     rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test  rdi, rdi
    jns   print_uint
    push  rdi
    mov   rdi, '-'
    call  print_char
    pop   rdi
    neg   rdi
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov   rax, rdi
    mov   r8, 10
    mov   r9, 1             ; sp offset
    dec   rsp
    mov   byte [rsp], 0     ; \0 -> stack
.loop:
    xor   rdx, rdx
    div   r8
    add   dl, '0'
    inc   r9
    dec   rsp
    mov   [rsp], dl
    test  rax, rax
    jne   .loop              ; quotient n    ot zero
.print:
    mov   rdi, rsp
    push  r9                 ; calling convention
    call  print_string
    pop   r9
    add   rsp, r9
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push  rbx
    xor   rax, rax
.loop:
    mov   cl, [rdi+rax]
    mov   bl, [rsi+rax]
    cmp   bl, cl
    jne   .neg
    test  bl, bl
    je    .pos
    inc   rax
    jmp   .loop
.pos:
    mov   rax, 1
    pop   rbx
    ret
.neg:
    xor   rax, rax
    pop   rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec       rsp
    mov       rsi, rsp
    mov       rax, SYS_READ
    xor       rdi, rdi
    mov       rdx, 1
    syscall
    test      rax, rax
    jz        .end
    xor       rax, rax
    mov al,   [rsp]
.end:
    inc       rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12              ; callee-saved registers
    push r13              ; used to safely store values
    push r14              ; w/ out pushing at each step
    mov  r12, rdi         ; ptr
    mov  r13, rsi         ; sz
    xor  r14, r14         ; length
.trim:
    call read_char
    cmp  al, ' '
    je   .trim
    cmp  al, `\t`
    je   .trim
    cmp  al, `\n`
    je   .trim
.loop:
    test al, al
    jz   .end
    cmp  r14, r13         ; buffer size exceeded
    je  .drop
    cmp  al, ' '
    je   .end
    cmp  al, `\t`
    je   .end
    cmp  al, `\n`
    je   .end
    mov  [r12+r14], al    ; rdi points to word_buf
    inc  r14
    call read_char
    jmp  .loop
.drop:
    xor  rax, rax
    pop  r12
    pop  r13
    pop  r14
    ret
.end:
    cmp  r14, r13
    je   .drop
    mov  rax, r12
    mov  byte [r12+r14], 0
    mov  rdx, r14
    pop  r12
    pop  r13
    pop  r14
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8,  r8
    mov r9,  10
.loop:
    mov cl,  [rdi+r8]
    cmp cl,  '0'
    jl  .end
    cmp cl,  '9'
    jg  .end
    sub cl,  '0'
    mul r9
    add rax, rcx
    inc r8
    jmp .loop
.end:
    mov rdx, r8
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov  cl,   [rdi]
    cmp  cl,   '9'
    jg   .drop
    cmp  cl,   '-'
    je   .neg
    cmp  cl,   '+'
    jge   .pos
.drop:
    xor  rdx,  rdx
    ret
.neg:
    inc  rdi
.pos:
    push cx
    call parse_uint
    pop  cx
    test rdx, rdx
    jz   .drop
    cmp  cl, '-'
    jne  .end
    neg  rax
    inc  rdx
    ret
.end:
    cmp  cl, '+'
    jne .real_end
    inc  rdx
.real_end:
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    je  .drop
    mov cl, [rdi+rax]
    mov [rsi+rax], cl
    inc rax
    test cl, cl
    jz .end
    jmp .loop
.drop:
    xor rax, rax
.end:
    ret
